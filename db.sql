-- -----------------------------------------------------
-- Schema crm
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `crm` ;

-- -----------------------------------------------------
-- Schema crm
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `crm` DEFAULT CHARACTER SET utf8 COLLATE utf8_slovenian_ci ;
USE `crm` ;

-- -----------------------------------------------------
-- Table `crm`.`Stranka`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`Stranka` ;

CREATE TABLE IF NOT EXISTS `crm`.`Stranka` (
  `idStranka` INT NOT NULL AUTO_INCREMENT,
  `ime` VARCHAR(45) NOT NULL,
  `priimek` VARCHAR(45) NOT NULL,
  `telefon` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idStranka`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crm`.`Sestanek`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`Sestanek` ;

CREATE TABLE IF NOT EXISTS `crm`.`Sestanek` (
  `idSestanek` INT NOT NULL AUTO_INCREMENT,
  `lokacija` VARCHAR(45) NOT NULL,
  `uraZacetka` TIME NOT NULL,
  `datumZacetka` DATE NOT NULL,
  `uraKonca` TIME NOT NULL,
  `datumKonca` DATE NOT NULL,
  PRIMARY KEY (`idSestanek`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crm`.`ZgodovinaSestankov`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`ZgodovinaSestankov` ;

CREATE TABLE IF NOT EXISTS `crm`.`ZgodovinaSestankov` (
  `idZgodovinaSestankov` INT NOT NULL AUTO_INCREMENT,
  `idStranka` INT NOT NULL,
  `idSestanek` INT NOT NULL,
  PRIMARY KEY (`idZgodovinaSestankov`),
  INDEX `fk_ZgodovinaSestankov_1_idx` (`idStranka` ASC),
  INDEX `fk_ZgodovinaSestankov_2_idx` (`idSestanek` ASC),
  CONSTRAINT `fk_ZgodovinaSestankov_1`
    FOREIGN KEY (`idStranka`)
    REFERENCES `crm`.`Stranka` (`idStranka`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ZgodovinaSestankov_2`
    FOREIGN KEY (`idSestanek`)
    REFERENCES `crm`.`Sestanek` (`idSestanek`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crm`.`Sklep`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`Sklep` ;

CREATE TABLE IF NOT EXISTS `crm`.`Sklep` (
  `idSklep` INT NOT NULL,
  `tip` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idSklep`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `crm`.`SklepihZaSestanek`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crm`.`SklepihZaSestanek` ;

CREATE TABLE IF NOT EXISTS `crm`.`SklepihZaSestanek` (
  `idSklepihZaSestanek` INT NOT NULL AUTO_INCREMENT,
  `idSestanek` INT NOT NULL,
  `idSklep` INT NOT NULL,
  PRIMARY KEY (`idSklepihZaSestanek`),
  INDEX `fk_SklepihZaSestanek_1_idx` (`idSestanek` ASC),
  INDEX `fk_SklepihZaSestanek_2_idx` (`idSklep` ASC),
  CONSTRAINT `fk_SklepihZaSestanek_1`
    FOREIGN KEY (`idSestanek`)
    REFERENCES `crm`.`Sestanek` (`idSestanek`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SklepihZaSestanek_2`
    FOREIGN KEY (`idSklep`)
    REFERENCES `crm`.`Sklep` (`idSklep`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


INSERT INTO `crm`.`Sklep` (`idSklep`, `tip`) VALUES
(1, "naslednji sestanek"),
(2, "pogodba"),
(3, "racun");

INSERT INTO `crm`.`Stranka` (`idStranka`, `ime`, `priimek`, `telefon`, `email`) VALUES
(1, "Vilko", "Scavnicar", "041-467-700", "VilkoScavnicar@gmail.com"),
(2, "Fidanco", "Beric", "031-105-556", "FidancoBeric@gmail.com"),
(3, "Patricija", "Jovetic", "070-522-925", "PatricijaJovetic@gmail.com"),
(4, "Zlatica", "Merdjadi", "051-515-966", "ZlaticaMerdjadi@gmail.com"),
(5, "Ana", "Zumeric", "031-696-167", "AnaZumeric@gmail.com");

INSERT INTO `crm`.`Sestanek` (`idSestanek`, `lokacija`, `uraZacetka`, `datumZacetka`, `uraKonca`, `datumKonca`) VALUES
(1, "Sejna soba 1", "10:00:00", "2018-04-13", "12:00:00", "2018-04-13"),
(2, "Sejna soba 1", "14:00:00", "2018-04-13", "15:00:00", "2018-04-13"),
(3, "Sejna soba 2", "22:00:00", "2018-04-13", "01:00:00", "2018-04-14"),
(4, "Sejna soba 1", "09:30:00", "2018-04-16", "10:00:00", "2018-04-16"),
(5, "Sejna soba 1", "13:00:00", "2018-04-16", "13:30:00", "2018-04-16"),
(6, "Sejna soba 3", "12:00:00", "2018-04-16", "12:30:00", "2018-04-16"),
(7, "Sejna soba 4", "12:00:00", "2018-04-16", "13:00:00", "2018-04-16");

INSERT INTO `crm`.`ZgodovinaSestankov` (`idZgodovinaSestankov`, `idStranka`, `idSestanek`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 3),
(4, 1, 4),
(5, 1, 5),
(6, 3, 6),
(7, 4, 7);

INSERT INTO `crm`.`SklepihZaSestanek` (`idSklepihZaSestanek`, `idSestanek`, `idSklep`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 2, 1),
(4, 3, 3),
(5, 4, 1),
(6, 5, 3),
(7, 6, 1),
(8, 7, 1);
