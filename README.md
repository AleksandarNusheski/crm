# CRM z Vaadin, JDBC in MySQL
Podjetje potrebuje preprost CRM (customer relationship management), s katerim bodo zaposleni vodili sestanke s svojimi strankami.

- Stranke imajo osebne (Ime, priimek, ...) in kontaktne podatke (telefon, email, ...).
- Za vsako stranko uporabnik beleži zgodovino sestankov. Vsak sestanek ima lokacijo, uro ter datum začetka in konca.
- Pri vsakem sestanku se določi en ali več sklepov. Sklep je lahko določitev naslednjega sestanka, pogodba ali pa račun.

Rešitev naj predvideva naslednje funkcionalnosti (implementiraj poljubne dve od njih):

- **Prikaz seznama strank. Seznam lahko omejimo z imenom in/ali priimkom.**
- Prikaz seznama sestankov za izbrano stranko. Seznam naj bo urejen kronološko (novejši na vrhu).
- Prikaz sklepov izbranega sestanka.
- **Dodajanje, urejanje in brisanje strank.**
- Dodajanje, urejanje in brisanje sestankov in sklepov.

#### model podatkovne baze
![baza](db.png)

## Navodila za postavitev razvojnega okolja in zagon aplikacije
V ukazna vrstica potrebno je se izvesti naslednje ukaze (predpostvljam da so git in mysql že inštalirane):
```
git clone https://AleksandarNusheski@bitbucket.org/AleksandarNusheski/crm.git
mysql -u root -e "create database crm" && mysql -u root -p crm < crm/db.sql
```
Če se ne nahajaš v mapo kjer je repozitorij crm, potrebno je podati celotna pot do sql skripta. Če uporabnik ima geslo, potrebno je dodati v crm\src\main\resources\application.properties naslednjo vrstico:
```
spring.datasource.password=geslo
```
To enako velja če je uporabnik imenovan drugače kot root (ne pozabi na prešnje ukaze). V mojem primeru uporabnik root, nima gesla.

Potem aplikacijo odpremo z poljuben IDE (kot na primer IntelliJ IDEA) in zaženemo aplikacijo (Run -> Run 'CrmApplication'). Aplikacija je dostopna na naslov http://localhost:8080/