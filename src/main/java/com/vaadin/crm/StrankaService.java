package com.vaadin.crm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StrankaService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    // Return all customers
    public List<Stranka> findAll() {
        return jdbcTemplate.query(
            "SELECT idStranka, ime, priimek, telefon, email FROM Stranka",
            (rs, rowNum) -> new Stranka(
                rs.getInt("idStranka"), rs.getString("ime"), rs.getString("priimek"),
                rs.getString("telefon"), rs.getString("email")
            )
        );
    }

    // Return only the customers with a matching first name
    public List<Stranka> findFilteredByName(String filter) {
        return jdbcTemplate.query(
            "SELECT idStranka, ime, priimek, telefon, email FROM Stranka WHERE ime LIKE ?",
            new Object[] {"%"+filter+"%"},
            (rs, rowNum) -> new Stranka(
                rs.getInt("idStranka"), rs.getString("ime"), rs.getString("priimek"),
                rs.getString("telefon"), rs.getString("email")
            )
        );
    }

    // Return only the customers with a matching last name
    public List<Stranka> findFilteredBySurname(String filter) {
        return jdbcTemplate.query(
            "SELECT idStranka, ime, priimek, telefon, email FROM Stranka WHERE priimek LIKE ?",
            new Object[] {"%"+filter+"%"},
            (rs, rowNum) -> new Stranka(
                rs.getInt("idStranka"), rs.getString("ime"), rs.getString("priimek"),
                rs.getString("telefon"), rs.getString("email")
            )
        );
    }

    // Return only the customers with a matching first and last name
    public List<Stranka> findFilteredByNameAndSurname(String filter1, String filter2) {
        return jdbcTemplate.query(
            "SELECT idStranka, ime, priimek, telefon, email FROM Stranka WHERE ime LIKE ? AND priimek LIKE ?",
            new Object[] {"%"+filter1+"%", "%"+filter2+"%"},
            (rs, rowNum) -> new Stranka(
                rs.getInt("idStranka"), rs.getString("ime"), rs.getString("priimek"),
                rs.getString("telefon"), rs.getString("email")
            )
        );
    }

    // Create a new customer in the database
    public void insert(Stranka stranka) {
        jdbcTemplate.update(
            "INSERT INTO Stranka (ime, priimek, telefon, email) VALUES (?, ?, ?, ?)",
            stranka.getIme(), stranka.getPriimek(), stranka.getTelefon(), stranka.getEmail()
        );
    }

    // Update the attributes of an already existing customer with a given ID
    public void update(Stranka stranka) {
        jdbcTemplate.update(
            "UPDATE Stranka SET ime=?, priimek=?, telefon=?, email=? WHERE idStranka=?",
            stranka.getIme(), stranka.getPriimek(), stranka.getTelefon(), stranka.getEmail(), stranka.getIdStranka()
        );
    }

    // Delete a customer for the database with a given ID
    public void delete(Stranka stranka) {
        jdbcTemplate.update(
            "DELETE FROM Stranka WHERE idStranka=?",
            stranka.getIdStranka()
        );
    }
}