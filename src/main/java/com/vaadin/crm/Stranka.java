package com.vaadin.crm;

public class Stranka {
    private int idStranka;
    private String ime, priimek, telefon, email;

    public Stranka(){}

    public Stranka(String ime, String priimek, String telefon, String email) {
        this.ime = ime;
        this.priimek = priimek;
        this.telefon = telefon;
        this.email = email;
    }

    public Stranka(int idStranka, String ime, String priimek, String telefon, String email) {
        this.idStranka = idStranka;
        this.ime = ime;
        this.priimek = priimek;
        this.telefon = telefon;
        this.email = email;
    }

    public int getIdStranka() {
        return idStranka;
    }

    public void setIdStranka(int idStranka) {
        this.idStranka = idStranka;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
