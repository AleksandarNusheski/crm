package com.vaadin.crm;

import com.vaadin.data.Binder;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

@SpringUI
public class MyUI extends UI {

    @Autowired
    private StrankaService service;
    private Stranka customer;
    private Binder<Stranka> binder = new Binder<>(Stranka.class);

    private TextField filterName = new TextField();
    private TextField filterSurname = new TextField();
    private Grid<Stranka> grid = new Grid(Stranka.class);
    private Label infoLabel = new Label();

    // Input fields for editing the already created customers
    private TextField ime = new TextField("Ime");
    private TextField priimek = new TextField("Priimek");
    private TextField telefon = new TextField("Telefon");
    private TextField email = new TextField("Email");
    private Button update = new Button("Posodobi", e -> updateCustomer());
    private Button delete = new Button("Izbrisi", e -> deleteCustomer());
    private Button cancelUpdate = new Button("Preklici", e -> { setUpdateFormVisible(false); grid.asSingleSelect().setValue(null); infoLabel.setValue(""); });

    // Input fields for creating a new customer
    private TextField imeInsert = new TextField("Ime");
    private TextField priimekInsert = new TextField("Priimek");
    private TextField telefonInsert = new TextField("Telefon");
    private TextField emailInsert = new TextField("Email");
    private Button create = new Button("Ustvari", e -> createCustomer());
    private Button clear = new Button("Pocisti", e -> clearForm());
    private Button cancelCreate = new Button("Preklici", e -> { setInsertFormVisible(false); infoLabel.setValue(""); });

    @Override
    protected void init(VaadinRequest request) {
        updateGrid();

        // Input fields to filter the customers in the database
        filterName.setPlaceholder("Filter po imenah");
        filterSurname.setPlaceholder("Filter po priimkih");
        filterName.addValueChangeListener(e -> filterGrid());
        filterSurname.addValueChangeListener(e -> filterGrid());
        // Set a time interval to limit the triggering of events when the user is typing
        filterName.setValueChangeMode(ValueChangeMode.LAZY);
        filterSurname.setValueChangeMode(ValueChangeMode.LAZY);

        // Button to clear the input filters
        Button clearFilterTextBtn = new Button("Zbrisi trenutne filtre");
        clearFilterTextBtn.addClickListener(e -> { filterName.clear(); filterSurname.clear(); });

        // Button to add a new customer
        Button addCustomerBtn = new Button("Ustvari nova stranka");
        addCustomerBtn.addClickListener(e -> createForm());

        // Group the filter input field and the clear button together and set a CSS class to them
        CssLayout toolbar = new CssLayout();
        toolbar.addComponents(filterName, filterSurname, clearFilterTextBtn, addCustomerBtn);
        toolbar.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

        // Set which attributes are going to be shown in the table
        grid.setColumns("ime", "priimek", "telefon", "email");
        grid.setSizeFull();
        grid.addSelectionListener(e -> updateForm()); // Choose a customer to edit him/her

        // Bind the input fields with the variables of the same class
        binder.bindInstanceFields(this);

        // Group the buttons
        CssLayout buttons = new CssLayout();
        buttons.addComponents(update, delete, cancelUpdate, create, clear, cancelCreate);
        buttons.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

        // Display the First and Last Name, Phone and Email fields of the both forms in a horizontal (not a vertical) layout
        HorizontalLayout imeINpriimek = new HorizontalLayout(ime, priimek, imeInsert, priimekInsert);
        HorizontalLayout telefonINemail = new HorizontalLayout(telefon, email, telefonInsert, emailInsert);

        // Display all elements in a vertical layout
        VerticalLayout layout = new VerticalLayout(
            toolbar, grid, infoLabel,
            imeINpriimek, telefonINemail, buttons
        );

        // Style for the elements (both forms in the center)
        layout.setComponentAlignment(infoLabel, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(imeINpriimek, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(telefonINemail, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);

        setContent(layout);
    }

    private void createCustomer() {
        customer = new Stranka(imeInsert.getValue(), priimekInsert.getValue(), telefonInsert.getValue(), emailInsert.getValue());
        service.insert(customer);
        infoLabel.setValue("Uspesno ste ustvarili nova stranka");
        clearForm();
        updateGrid();
    }

    private void updateCustomer() {
        service.update(customer);
        infoLabel.setValue("Uspesno ste posodobili obstojeco stranko");
        updateGrid();
    }

    private void deleteCustomer() {
        service.delete(customer);
        infoLabel.setValue("Uspesno ste zbrisali obstojeco stranko");
        updateGrid();
    }

    private void updateGrid() {
        List<Stranka> customers = service.findAll();
        grid.setItems(customers);
        setUpdateFormVisible(false);
        setInsertFormVisible(false);
    }

    private void filterGrid() {
        List<Stranka> customers;
        String fName = filterName.getValue();
        String fSurname = filterSurname.getValue();

        if(fName == null || fName.isEmpty()) {
            customers = service.findFilteredBySurname(fSurname);
        } else if(fSurname == null || fSurname.isEmpty()) {
            customers = service.findFilteredByName(fName);
        } else {
            customers = service.findFilteredByNameAndSurname(fName, fSurname);
        }
        grid.setItems(customers);
        setUpdateFormVisible(false);
    }

    private void updateForm() {
        // Hide the Add Form
        setInsertFormVisible(false);

        // Toggle the Edit Form
        if (grid.asSingleSelect().isEmpty()) {
            setUpdateFormVisible(false);
        } else {
            customer = grid.asSingleSelect().getValue();
            binder.setBean(customer);
            setUpdateFormVisible(true);
        }
    }

    private void createForm() {
        // Hide the Edit Form
        setUpdateFormVisible(false);

        // Reset the toggle of the Edit Form
        grid.asSingleSelect().setValue(null);

        // Show the Add Form
        setInsertFormVisible(true);

        // Clear the Add Form of previous inputs
        clearForm();
    }

    private void clearForm() {
        imeInsert.setValue("");
        priimekInsert.setValue("");
        telefonInsert.setValue("");
        emailInsert.setValue("");
    }

    // Set the visibility of the Edit Form fields
    private void setUpdateFormVisible(boolean visible) {
        ime.setVisible(visible);
        priimek.setVisible(visible);
        telefon.setVisible(visible);
        email.setVisible(visible);
        update.setVisible(visible);
        delete.setVisible(visible);
        cancelUpdate.setVisible(visible);
    }

    // Set the visibility of the Add Form fields
    private void setInsertFormVisible(boolean visible) {
        imeInsert.setVisible(visible);
        priimekInsert.setVisible(visible);
        telefonInsert.setVisible(visible);
        emailInsert.setVisible(visible);
        create.setVisible(visible);
        clear.setVisible(visible);
        cancelCreate.setVisible(visible);
    }
}
